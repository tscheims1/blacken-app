package ch.tscheims.blackenapp;

/**
 * Created by tsche on 22.09.2017.
 */
import org.junit.Test;
import org.la4j.Vector;


import static org.junit.Assert.assertEquals;

public class GpsCoordinateTest
{
    @Test
    public void checkIfGpsCartesianTransformation()
    {
        GpsCoordinate coordinate = new GpsCoordinate(45,7,110);
        Vector cartCoordinate = coordinate.toCartesianCoordinate();
        GpsCoordinate coordinate1 = new GpsCoordinate(cartCoordinate);

        assertEquals(coordinate.getAlt(),coordinate1.getAlt(),0.00001);
        assertEquals(coordinate.getLat(),coordinate1.getLat(),0.00001);
        assertEquals(coordinate.getLng(),coordinate1.getLng(),0.00001);

    }
    @Test
    public void testHeadingCalculation()
    {
        GpsCoordinate KansasCity = new GpsCoordinate(39.099912,-94.581213,0);
        GpsCoordinate StLuis = new GpsCoordinate(38.627089, -90.200203,0);


        assertEquals(KansasCity.CalculateHeading(StLuis),96.51,0.01);
    }
}
