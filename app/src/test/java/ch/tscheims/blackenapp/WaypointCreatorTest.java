package ch.tscheims.blackenapp;

/**
 * Created by tsche on 24.09.2017.
 */

import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Queue;

import ch.tscheims.blackenapp.GpsCoordinate;
import ch.tscheims.blackenapp.WaypointsCreator;

import static org.junit.Assert.assertEquals;

public class WaypointCreatorTest
{
    @Test
    public void TestSimplePolygon()
    {
        ArrayList<GpsCoordinate> polygon = new ArrayList<>();
        final float baseAltitude = 15f;

       /* polygon.add(new GpsCoordinate(46.88762, 7.659930,baseAltitude));
        polygon.add(new GpsCoordinate(46.88833, 7.660182,baseAltitude));
        polygon.add(new GpsCoordinate(46.88978, 7.659557,baseAltitude));
        polygon.add(new GpsCoordinate(46.88982, 7.657883,baseAltitude));
*/
        polygon.add(new GpsCoordinate(46.88723, 7.659992, baseAltitude));
        polygon.add(new GpsCoordinate(46.88766, 7.660196, baseAltitude));
        polygon.add(new GpsCoordinate(46.88838, 7.659338, baseAltitude));
        //polygon.add(new GpsCoordinate(46.88800, 7.657416,baseAltitude));
        polygon.add(new GpsCoordinate(46.887808, 7.657855,baseAltitude));
        polygon.add(new GpsCoordinate(46.88748, 7.657893,baseAltitude));
        polygon.add(new GpsCoordinate(46.88708, 7.658730,baseAltitude));

        WaypointsCreator wpCreator = new WaypointsCreator(polygon,0);
        wpCreator.createWaypointsForMission(2,2);
        String waypoints = wpCreator.toJson();
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("waypointexport_2x2.csv", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.print(wpCreator.toCsv());
        writer.close();

        wpCreator.createWaypointsForMission(4,4);

        try {
            writer = new PrintWriter("waypointexport_4x4.csv", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

        }
        String _4x4 = wpCreator.toJson();
        writer.print(wpCreator.toCsv());

        writer.close();

        wpCreator.createWaypointsForMission(10,10);

        try {
            writer = new PrintWriter("waypointexport_10x10.csv", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

        }
        writer.print(wpCreator.toCsv());
        String _10x10 = wpCreator.toJson();

        writer.close();

    }
}
