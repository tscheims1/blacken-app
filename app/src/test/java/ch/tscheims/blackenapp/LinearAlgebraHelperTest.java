package ch.tscheims.blackenapp;

/**
 * Created by tsche on 22.09.2017.
 */
import org.junit.Test;
import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;

import ch.tscheims.blackenapp.LinearAlgebraHelper;

import static org.junit.Assert.assertEquals;


public class LinearAlgebraHelperTest {


    @Test
    public void checkIfIntersects()
    {
        Vector origin = new BasicVector(new double[]{0,0,0});
        Vector dir = new BasicVector(new double[]{1,1,0});

        Vector start = new BasicVector(new double[]{5,0,0});
        Vector end = new BasicVector(new double[]{0,10,0});

        Vector intersectionPoint = LinearAlgebraHelper.getRayLineSegmentIntersection(origin,dir,start,end);
        assertEquals(3.3333333,intersectionPoint.get(0),0.00001);
        assertEquals(3.3333333,intersectionPoint.get(1),0.00001);
        assertEquals(0,intersectionPoint.get(2),0.00001);
    }
    @Test
    public void checkIfNotIntersectsWithinLineSegment()
    {
        Vector origin = new BasicVector(new double[]{0,0,0});
        Vector dir = new BasicVector(new double[]{1,1,0});

        Vector start = new BasicVector(new double[]{5,0,0});
        Vector end = new BasicVector(new double[]{4,1,0});

        Vector intersectionPoint = LinearAlgebraHelper.getRayLineSegmentIntersection(origin,dir,start,end);
        assertEquals(null,intersectionPoint);


    }
    @Test
    public void checkIfNotIntersects()
    {
        Vector origin = new BasicVector(new double[]{0,0,0});
        Vector dir = new BasicVector(new double[]{1,1,0});

        Vector start = new BasicVector(new double[]{5,0,0});
        Vector end = new BasicVector(new double[]{10,5,0});

        Vector intersectionPoint = LinearAlgebraHelper.getRayLineSegmentIntersection(origin,dir,start,end);
        assertEquals(null,intersectionPoint);
    }

}
