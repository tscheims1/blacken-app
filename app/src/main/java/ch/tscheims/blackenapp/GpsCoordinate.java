package ch.tscheims.blackenapp;

import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;

/**
 * Created by tsche on 22.09.2017.
 */

public class GpsCoordinate {

    static final int EARTH_RADIUS = 6371000; // in meters

    //from https://stackoverflow.com/questions/1185408/converting-from-longitude-latitude-to-cartesian-coordinates
    // spherical coordinate transformation.

    private static final double EPSILON = 0.0000001;
    public GpsCoordinate(double lat,double lng, double alt)
    {
        this.lat = lat;
        this.lng = lng;
        this.alt = alt;
    }
    public GpsCoordinate(Vector cartCoordinate)
    {
        double x = cartCoordinate.get(0);
        double y =  cartCoordinate.get(1);
        double z = cartCoordinate.get(2);

        double rho = cartCoordinate.norm();

        lat = Math.toDegrees(Math.asin(z / rho));
        lng = Math.toDegrees(Math.atan2(y, x));
        alt = rho - EARTH_RADIUS;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getAlt() {
        return alt;
    }

    public void setAlt(double alt) {
        this.alt = alt;
    }

    double lat;
    double lng;
    double alt;

    public boolean equals(Object other)
    {
        if(other == null)
            return false;

        GpsCoordinate otherCoordinate = (GpsCoordinate)other;
        return(Math.abs(otherCoordinate.lat-  lat) < EPSILON  && Math.abs(otherCoordinate.lng - lng) < EPSILON && Math.abs(otherCoordinate.alt - alt) < EPSILON);
    }
    public Vector toCartesianCoordinate()
    {
        double lat = Math.toRadians(this.lat);
        double lng = Math.toRadians(this.lng);

        double x = (EARTH_RADIUS+alt) * Math.cos(lat)* Math.cos(lng);
        double y = (EARTH_RADIUS+alt) *Math.cos(lat)* Math.sin(lng);
        double z = (EARTH_RADIUS+alt) * Math.sin(lat);
        return new BasicVector(new double[]{x,y,z});

    }
    public double CalculateHeading(GpsCoordinate other)
    {
        double latA = Math.toRadians(lat);
        double latB = Math.toRadians(other.lat);
        double lngA = Math.toRadians(lng);
        double lngB = Math.toRadians(other.lng);

        double x = Math.cos(latB)*Math.sin(lngB-lngA);
        double y = Math.cos(latA)*Math.sin(latB)-
                Math.sin(latA)*
                        Math.cos(latB)*Math.cos(
                        lngB-lngA);

        return Math.toDegrees(Math.atan2(x,y));

    }
}
