package ch.tscheims.blackenapp;

import android.support.annotation.Nullable;


import java.util.ArrayList;
import java.util.List;

import dji.common.error.DJIError;
import dji.common.mission.waypoint.Waypoint;
import dji.common.mission.waypoint.WaypointAction;
import dji.common.mission.waypoint.WaypointActionType;
import dji.common.mission.waypoint.WaypointMission;
import dji.common.mission.waypoint.WaypointMissionFinishedAction;
import dji.common.mission.waypoint.WaypointMissionFlightPathMode;
import dji.common.mission.waypoint.WaypointMissionGotoWaypointMode;
import dji.common.mission.waypoint.WaypointMissionHeadingMode;
import dji.sdk.flightcontroller.Compass;
import dji.sdk.flightcontroller.FlightController;
import dji.sdk.mission.MissionControl;
import dji.sdk.mission.timeline.Mission;
import dji.sdk.mission.timeline.TimelineElement;
import dji.sdk.mission.timeline.TimelineEvent;

/**
 * Created by tsche on 28.09.2017.
 */

public class TimelineMissionCreator
{
    private  MissionControl missionControl;
    private FlightController flightController;
    private final int WAYPOINTS_PER_MISSION = 3;
    private int wpDistance;
    private int shiftingSize;
    private float speed;
    private float altitude;

    public TimelineMissionCreator(FlightController flightController,float speed,float altitude,int shiftingSize,int wpDistance)
    {
        this.flightController = flightController;
        this.speed = speed;
        this.shiftingSize = shiftingSize;
        this.wpDistance = wpDistance;
        this.altitude = altitude;
    }

    public List<TimelineElement> createTimelineMission(ArrayList<GpsCoordinate> polygon)
    {
        List<TimelineElement> timelineElements = new ArrayList<>();
        missionControl = MissionControl.getInstance();

        Compass compass = flightController.getCompass();
        int heading = (int) compass.getHeading();


        WaypointsCreator wp = new WaypointsCreator(polygon, altitude);
        wp.createWaypointsForMission(shiftingSize,wpDistance);
        double desiredHeading = wp.getHeading();
        List<GpsCoordinate> gpsCoordinates = wp.getWaypoints();




        for(int currentWaypointPosition = 0;currentWaypointPosition < gpsCoordinates.size();)
        {
            WaypointMission.Builder builder = new WaypointMission.Builder();

            builder.autoFlightSpeed(speed);
            builder.maxFlightSpeed(speed);
            builder.setExitMissionOnRCSignalLostEnabled(false);
            builder.finishedAction(WaypointMissionFinishedAction.NO_ACTION);
            builder.flightPathMode(WaypointMissionFlightPathMode.NORMAL);
            builder.gotoFirstWaypointMode(WaypointMissionGotoWaypointMode.SAFELY);
            builder.headingMode(WaypointMissionHeadingMode.USING_INITIAL_DIRECTION);
            builder.repeatTimes(1);

            List<Waypoint> wpList = new ArrayList<>();
            for (int i = 0; i < WAYPOINTS_PER_MISSION && currentWaypointPosition+i<gpsCoordinates.size(); i++) {

                Waypoint waypoint = new Waypoint(gpsCoordinates.get(currentWaypointPosition+i).getLat(), gpsCoordinates.get(currentWaypointPosition+i).getLng(),
                        (float) gpsCoordinates.get(currentWaypointPosition+i).getAlt());

                waypoint.addAction(new WaypointAction(WaypointActionType.ROTATE_AIRCRAFT,(int)desiredHeading-heading));
                waypoint.addAction(new WaypointAction(WaypointActionType.GIMBAL_PITCH, -90));
                waypoint.addAction(new WaypointAction(WaypointActionType.START_TAKE_PHOTO, 1));
                wpList.add(waypoint);
            }
            builder.waypointList(wpList).waypointCount(wpList.size());
            TimelineElement te = Mission.elementFromWaypointMission(builder.build());

            timelineElements.add(te);

            currentWaypointPosition+=WAYPOINTS_PER_MISSION;

        }
        if (missionControl.scheduledCount() > 0) {
            missionControl.unscheduleEverything();
            missionControl.removeAllListeners();
        }

        MissionControl.Listener listener = new MissionControl.Listener() {
            @Override
            public void onEvent(@Nullable TimelineElement element, TimelineEvent event, DJIError error) {


            }

        };
        missionControl.addListener(listener);
        missionControl.scheduleElement(timelineElements.get(0));

        return timelineElements;
    }
}
