package ch.tscheims.blackenapp;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dji.GSDemo.GoogleMap.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.la4j.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;


import dji.common.flightcontroller.FlightControllerState;
import dji.common.mission.waypoint.WaypointMissionDownloadEvent;
import dji.common.mission.waypoint.WaypointMissionExecutionEvent;
import dji.common.mission.waypoint.WaypointMissionUploadEvent;
import dji.common.useraccount.UserAccountState;
import dji.common.util.CommonCallbacks;
import dji.sdk.base.BaseProduct;
import dji.sdk.flightcontroller.FlightController;
import dji.common.error.DJIError;
import dji.sdk.mission.MissionControl;
import dji.sdk.mission.timeline.TimelineElement;
import dji.sdk.mission.waypoint.WaypointMissionOperator;
import dji.sdk.mission.waypoint.WaypointMissionOperatorListener;
import dji.sdk.products.Aircraft;
import dji.sdk.sdkmanager.DJISDKManager;
import dji.sdk.useraccount.UserAccountManager;

public class MainActivity extends FragmentActivity implements View.OnClickListener, GoogleMap.OnMapClickListener, OnMapReadyCallback {

    protected static final String TAG = "Blacken-App";

    private final int polygonColor = 0xFF0000FF;
    private final int flyingCurveColor = 0xFFFF0000;
    private final int flyingCurveUpdateIntervall = 2*1000;
    private AtomicInteger sentBatchNo = new AtomicInteger(-1);
    private LatLng lastcurvePos= null;

    private final int droneIdleCheckIntervall = 20*1000;


    private long lastDronepositionCheck = 0;
    private List<Double> positionLatContainer = new ArrayList<>();
    private List<Double> positionLngContainer = new ArrayList<>();

    private AtomicBoolean missionStarted = new AtomicBoolean();
    private long lastUpdateTime = 0;
    private GoogleMap gMap;
    private MissionControl missionControl;
    private AtomicBoolean forceStopped = new AtomicBoolean(false);
    private Button locate, config, resend;
    private Button  nextBatch, start, stop,createMission;
    private int currentTimeLineElement = 0;
    private boolean isAdd = false;
    private double droneLocationLat = 181, droneLocationLng = 181;
    private final Map<Integer, Marker> mMarkers = new ConcurrentHashMap<Integer, Marker>();
    private Marker droneMarker = null;
    private List<TimelineElement> timelineElements = new ArrayList<>();

    private float droneSpeed = 3.0f;
    private float droneAltitude = 5f;
    private int droneShiftingSize = 2;
    private int droneWpDistance = 2;

    private FlightController flightController;
    private WaypointMissionOperator instance;

    @Override
    protected void onResume(){
        super.onResume();
        initFlightController();
    }

    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onDestroy(){
        unregisterReceiver(mReceiver);
        removeListener();
        super.onDestroy();
    }

    private void setResultToToast(final String string){
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, string, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initUI() {

        locate = (Button) findViewById(R.id.locate);
        config = (Button) findViewById(R.id.config);
        resend = (Button) findViewById(R.id.resend);
        nextBatch = (Button) findViewById(R.id.nextbatch);
        start = (Button) findViewById(R.id.start);
        stop = (Button) findViewById(R.id.stop);
        createMission =(Button)findViewById(R.id.createMission);
        locate.setOnClickListener(this);
        config.setOnClickListener(this);
        resend.setOnClickListener(this);
        nextBatch.setOnClickListener(this);
        start.setOnClickListener(this);
        stop.setOnClickListener(this);
        createMission.setOnClickListener(this);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // When the compile and target version is higher than 22, please request the
        // following permissions at runtime to ensure the
        // SDK work well.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.VIBRATE,
                            Manifest.permission.INTERNET, Manifest.permission.ACCESS_WIFI_STATE,
                            Manifest.permission.WAKE_LOCK, Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.CHANGE_WIFI_STATE, Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS,
                            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.SYSTEM_ALERT_WINDOW,
                            Manifest.permission.READ_PHONE_STATE,
                    }
                    , 1);
        }

        setContentView(R.layout.activity_main);

        IntentFilter filter = new IntentFilter();
        filter.addAction(DjiConnector.FLAG_CONNECTION_CHANGE);
        registerReceiver(mReceiver, filter);

        initUI();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        missionControl = MissionControl.getInstance();
        addListener();

    }

    protected BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            onProductConnectionChange();
        }
    };

    private void onProductConnectionChange()
    {
        initFlightController();
        loginAccount();
    }

    private void loginAccount(){

        UserAccountManager.getInstance().logIntoDJIUserAccount(this,
                new CommonCallbacks.CompletionCallbackWith<UserAccountState>() {
                    @Override
                    public void onSuccess(final UserAccountState userAccountState) {
                        Log.e(TAG, "Login Success");
                    }
                    @Override
                    public void onFailure(DJIError error) {
                        setResultToToast("Login Error:"
                                + error.getDescription());
                    }
                });
    }

    private void initFlightController() {

        BaseProduct product = DjiConnector.getProductInstance();
        if (product != null && product.isConnected()) {
            if (product instanceof Aircraft) {
                flightController = ((Aircraft) product).getFlightController();
            }
        }

        if (flightController != null) {
            flightController.setStateCallback(new FlightControllerState.Callback() {

                @Override
                public void onUpdate(FlightControllerState djiFlightControllerCurrentState) {
                    droneLocationLat = djiFlightControllerCurrentState.getAircraftLocation().getLatitude();
                    droneLocationLng = djiFlightControllerCurrentState.getAircraftLocation().getLongitude();
                    updateDroneLocation();
                }
            });
        }
    }

    //Add Listener for WaypointMissionOperator
    private void addListener() {
        if (getWaypointMissionOperator() != null){
            getWaypointMissionOperator().addListener(eventNotificationListener);
        }
    }

    private void removeListener() {
        if (getWaypointMissionOperator() != null) {
            getWaypointMissionOperator().removeListener(eventNotificationListener);
        }
    }

    private WaypointMissionOperatorListener eventNotificationListener = new WaypointMissionOperatorListener() {
        @Override
        public void onDownloadUpdate(WaypointMissionDownloadEvent downloadEvent) {
        }

        @Override
        public void onUploadUpdate(WaypointMissionUploadEvent uploadEvent)
        {
            if(uploadEvent != null && uploadEvent.getProgress() != null)
                setResultToToast("upload mission to device. waypoint no "+uploadEvent.getProgress().uploadedWaypointIndex +" uploaded");
        }

        @Override
        public void onExecutionUpdate(WaypointMissionExecutionEvent executionEvent) {
           }

        @Override
        public void onExecutionStart() {

        }

        @Override
        public   void   onExecutionFinish(@Nullable final DJIError error) {
            setResultToToast("finished batch: " + currentTimeLineElement);


            if(!forceStopped.get()) {
                new Thread() {
                    public void run() {
                        if(currentTimeLineElement+1 >= timelineElements.size())
                        {
                            missionStarted.set(false);
                            return;
                        }

                            try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {

                            e.printStackTrace();
                        }
                        if (missionControl.scheduledCount() > 0)
                            missionControl.unscheduleEverything();

                        currentTimeLineElement++;

                        missionControl.scheduleElement(timelineElements.get(currentTimeLineElement));

                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {

                            e.printStackTrace();
                        }
                        missionControl.startTimeline();
                        sentBatchNo.set(currentTimeLineElement);

                    }
                }.start();
            }
            forceStopped.set(false);
        }
    };

    public WaypointMissionOperator getWaypointMissionOperator() {
        if (instance == null) {
            instance = DJISDKManager.getInstance().getMissionControl().getWaypointMissionOperator();
        }
        return instance;
    }

    private void setUpMap() {
        gMap.setOnMapClickListener(this);// add the listener for click for amap object

    }

    @Override
    public void onMapClick(LatLng point) {
        if (isAdd == true) {
            markWaypoint(point);
        }

    }

    public static boolean checkGpsCoordination(double latitude, double longitude) {
        return (latitude > -90 && latitude < 90 && longitude > -180 && longitude < 180) && (latitude != 0f && longitude != 0f);
    }

    // Update the drone location based on states from MCU.
    private void updateDroneLocation(){

        LatLng pos = new LatLng(droneLocationLat, droneLocationLng);
        long currentTime = System.currentTimeMillis();

        if(lastDronepositionCheck + droneIdleCheckIntervall > currentTime)
        {
                GpsCoordinate coordinate = new GpsCoordinate(pos.latitude,pos.longitude,0);
                Vector cartCoordinate = coordinate.toCartesianCoordinate();
                positionLatContainer.add(cartCoordinate.get(0));
                positionLngContainer.add(cartCoordinate.get(1));
        }
        else
        {
            if(positionLatContainer.size() >0)
            {
                double stdDeviationLat = MathHelper.GetStdDeviation(positionLatContainer);
                double stdDeviationLng = MathHelper.GetStdDeviation(positionLngContainer);

                double maxStdDeviation = Math.max(stdDeviationLat, stdDeviationLng);
              

                positionLatContainer.clear();
                positionLngContainer.clear();
            }
            lastDronepositionCheck = currentTime;
        }


        //update coordinates for google maps
        if(lastUpdateTime +flyingCurveUpdateIntervall < currentTime)
        {
            if(lastcurvePos == null)
                lastcurvePos = pos;
            else
            {

            }
        }



        //Create MarkerOptions object
        final MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(pos);
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.aircraft));

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (droneMarker != null) {
                    droneMarker.remove();
                }

                if (checkGpsCoordination(droneLocationLat, droneLocationLng)) {
                    droneMarker = gMap.addMarker(markerOptions);
                }

            }
        });
    }

    private void markWaypoint(LatLng point){
        //Create MarkerOptions object
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(point);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        Marker marker = gMap.addMarker(markerOptions);
        mMarkers.put(mMarkers.size(), marker);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.locate:{
                updateDroneLocation();
                cameraUpdate(); // Locate the drone's place
                break;
            }
            case R.id.config:{
                createConfigDialog();
                //enableDisableAdd();
                break;
            }
            case R.id.resend:
                if(missionControl.scheduledCount() >0)
                {
                    missionControl.startTimeline();
                }
                break;
            case R.id.nextbatch:
            {

                sendNextBatch();
                break;
            }
            case R.id.start:
            {
                missionStarted.set(true);
                missionControl.startTimeline();
                forceStopped.set(false);
                break;
            }
            case R.id.stop:
            {
                missionStarted.set(false);
                forceStopped.set(true);
                missionControl.stopTimeline();
                break;
            }
            case R.id.createMission: {
                //calculate all necessary waypoints
                ArrayList<GpsCoordinate> polygon = new ArrayList<>();
                final float baseAltitude = 5f;
                polygon.add(new GpsCoordinate(46.88723, 7.659992, baseAltitude));
                polygon.add(new GpsCoordinate(46.88766, 7.660196, baseAltitude));
                polygon.add(new GpsCoordinate(46.88838, 7.659338, baseAltitude));
                TimelineMissionCreator creator = new TimelineMissionCreator(flightController,droneSpeed,droneAltitude,droneShiftingSize,droneWpDistance);
                timelineElements = creator.createTimelineMission(polygon);

                PolygonOptions polygonOptions = new PolygonOptions();
                for(GpsCoordinate coordinate : polygon)
                {
                    polygonOptions.add(new LatLng(coordinate.getLat(),coordinate.getLng()));
                }
                polygonOptions.strokeColor(polygonColor);
                polygonOptions.strokeWidth(3);
                gMap.addPolygon(polygonOptions);

                break;
            }
            default:
                break;
        }
    }

    private void cameraUpdate(){
        LatLng pos = new LatLng(droneLocationLat, droneLocationLng);
        float zoomlevel = (float) 18.0;
        CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(pos, zoomlevel);
        gMap.moveCamera(cu);

    }
    boolean isIntValue(String val)
    {
        try {
            val=val.replace(" ","");
            Integer.parseInt(val);
        } catch (Exception e) {return false;}
        return true;
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (gMap == null) {
            gMap = googleMap;
            setUpMap();
        }

        gMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(droneLocationLat,droneLocationLng)));
    }
    private void sendNextBatch() {
        if(currentTimeLineElement != sentBatchNo.get())
        {

            if(currentTimeLineElement< timelineElements.size())
            {

                currentTimeLineElement++;
                sentBatchNo.set(currentTimeLineElement);
                setResultToToast("send next batch: "+currentTimeLineElement);
                missionControl.stopTimeline();
                if (missionControl.scheduledCount() > 0)
                    missionControl.unscheduleEverything();
                missionControl.scheduleElement(timelineElements.get(currentTimeLineElement));
                missionControl.startTimeline();
                missionStarted.set(true);
            }
        }
        else
        {
            setResultToToast("resend Batch: "+ currentTimeLineElement);
            missionStarted.set(false);
            forceStopped.set(true);
            missionControl.stopTimeline();
            missionControl.unscheduleEverything();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            sentBatchNo.set(currentTimeLineElement);
            missionControl.scheduleElement(timelineElements.get(currentTimeLineElement));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            missionControl.startTimeline();
            missionStarted.set(true);
            forceStopped.set(false);
        }
    }
    public void createConfigDialog()
    {
        LinearLayout wayPointSettings = (LinearLayout)getLayoutInflater().inflate(R.layout.dialog_waypointsetting, null);
        final TextView wpAltitude_TV = (TextView) wayPointSettings.findViewById(R.id.altitude);
        RadioGroup speed_RG = (RadioGroup) wayPointSettings.findViewById(R.id.speed);
        final TextView wpDistance_TV = (TextView)wayPointSettings.findViewById(R.id.waypointDistance);
        final  TextView wpShiftSize_TV= (TextView) wayPointSettings.findViewById(R.id.shiftSize);

        wpAltitude_TV.setText(Float.toString(droneAltitude));
        wpDistance_TV.setText(Integer.toString(droneWpDistance));
        wpShiftSize_TV.setText(Integer.toString(droneShiftingSize));
        if(droneSpeed == 3.0f)
            speed_RG.check(R.id.lowSpeed);
        else if(droneSpeed == 5.0f)
            speed_RG.check(R.id.MidSpeed);
        else
            speed_RG.check(R.id.HighSpeed);


        speed_RG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.lowSpeed){
                    droneSpeed = 3.0f;
                } else if (checkedId == R.id.MidSpeed){
                    droneSpeed = 5.0f;
                } else if (checkedId == R.id.HighSpeed){
                    droneSpeed = 10.0f;
                }
            }

        });
        new AlertDialog.Builder(this)
                .setTitle("")
                .setView(wayPointSettings)
                .setPositiveButton("Finish",new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id) {

                        String altitudeString = wpAltitude_TV.getText().toString();
                        String shiftingSizeString = wpShiftSize_TV.getText().toString();
                        String wpDistanceString = wpDistance_TV.getText().toString();
                        droneAltitude = Integer.parseInt(nulltoIntegerDefalt(altitudeString));
                        droneShiftingSize = Integer.parseInt(nulltoIntegerDefalt(wpDistanceString));
                        droneWpDistance = Integer.parseInt(nulltoIntegerDefalt(wpDistanceString));

                    }

                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }

                })
                .create()
                .show();
    }
    String nulltoIntegerDefalt(String value){
        if(!isIntValue(value)) value="0";
        return value;
    }
}
