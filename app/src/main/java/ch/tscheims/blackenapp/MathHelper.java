package ch.tscheims.blackenapp;

import java.util.List;

/**
 * Created by tscheims on 29.09.2017.
 */

public class MathHelper
{
    public static double GetStdDeviation(List<Double> numbers)
    {
        double mean = 0;
        for(double number:numbers)
        {
            mean += number;
        }
        mean /= numbers.size();
        double variant =0;
        for(double number:numbers)
        {
            variant += (number -mean)*(number -mean);
        }
        variant /= numbers.size();
        return Math.sqrt(variant);

    }
}
