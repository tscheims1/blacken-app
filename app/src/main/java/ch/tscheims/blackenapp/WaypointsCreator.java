package ch.tscheims.blackenapp;

import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;

import java.util.ArrayList;


/**
 * Created by tsche on 22.09.2017.
 */

public class WaypointsCreator
{



    ArrayList<Vector> polygonPoints = new ArrayList<>();
    ArrayList<GpsCoordinate> waypointsForMission = new ArrayList<>();

    Vector centerOfMass = Vector.zero(3);

    public WaypointsCreator(ArrayList<GpsCoordinate>polygon,double altitude)
    {

        for(GpsCoordinate coordinate : polygon)
        {
            coordinate.setAlt(coordinate.getAlt()+altitude);
            polygonPoints.add(coordinate.toCartesianCoordinate());
        }
        for(Vector polygonPoint : polygonPoints)
        {
            centerOfMass = centerOfMass.add(polygonPoint);
        }

        centerOfMass = centerOfMass.divide(polygonPoints.size());
        for(int i = 0; i < polygonPoints.size(); i++)
        {
          polygonPoints.set(i,polygonPoints.get(i).subtract(centerOfMass));
        }


    }
    public void createWaypointsForMission()
    {
        createWaypointsForMission(1,1);
    }
    public void createWaypointsForMission(int shiftingSize,int wayPointDistance)
    {
        waypointsForMission = new ArrayList<>();
        Vector center = Vector.zero(3);
        for(Vector polygonPoint : polygonPoints)
        {
            center = center.add(polygonPoint);

        }
        center = center.divide(polygonPoints.size());


        Vector originPoint = polygonPoints.get(0);
        Vector currentPos = originPoint;
        Vector shiftVector = null;
        int currentIntersectionLineNo = 0;
        Vector flyingDirection = polygonPoints.get(1).subtract(polygonPoints.get(0));




        waypointsForMission.add(new GpsCoordinate(originPoint.add(centerOfMass)));


        for(;;)
        {
            Vector intersection = null;
            for (int i = 0; i < polygonPoints.size(); i++)
            {
                Vector lineSegmentStart;
                Vector lineSegmentEnd;
                if (i + 1 < polygonPoints.size()) {
                    lineSegmentStart = polygonPoints.get(i);
                    lineSegmentEnd = polygonPoints.get(i + 1);
                }
                else
                {
                    lineSegmentStart = polygonPoints.get(i);
                    lineSegmentEnd = polygonPoints.get(0);
                }
                //because of floating point calculation: matching the polygon edges are difficult
                //so we add a little gap
                Vector lineSegmentdir = lineSegmentEnd.subtract(lineSegmentStart);
                Vector tmpStartSegment  = lineSegmentStart.add(lineSegmentdir.multiply(-0.001));
                Vector tmpEndSegment = lineSegmentStart.add(lineSegmentdir.multiply(1.001));
                lineSegmentStart = tmpStartSegment;
                lineSegmentEnd = tmpEndSegment;
                ///

                intersection = LinearAlgebraHelper.getRayLineSegmentIntersection(currentPos,flyingDirection, lineSegmentStart, lineSegmentEnd);
                if (intersection != null && !intersection.equals(currentPos) && currentIntersectionLineNo != i )
                {
                    currentIntersectionLineNo = i;

                    Vector wayPointDir = intersection.subtract(currentPos);
                    double wayPointLength = wayPointDir.norm();
                    double wayPointFraction = wayPointDistance/wayPointLength;
                    for(int k =1; k*wayPointFraction< 1;k++)
                    {
                        waypointsForMission.add(new GpsCoordinate(currentPos.add(wayPointDir.multiply(k*wayPointFraction)).add(centerOfMass)));
                    }
                    waypointsForMission.add(new GpsCoordinate(intersection.add(centerOfMass)));

                    Vector nextDirection = lineSegmentEnd.subtract(lineSegmentStart);
                    double sinAlpha = flyingDirection.divide(flyingDirection.norm()).
                            innerProduct(nextDirection.divide(nextDirection.norm()));
                    Vector projectedVector =  (flyingDirection.divide(flyingDirection.norm()).multiply(nextDirection.norm()*sinAlpha));


                    /* first time we calculate the shift vector ->
                      check if the new Waypoint is inside the polygon
                     */

                    Vector tmpShiftVector = nextDirection.subtract(projectedVector);
                    if(shiftVector == null)
                    {
                        Vector isShiftVectorOutsidePolygon = LinearAlgebraHelper.
                                getRayLineSegmentIntersection(currentPos, flyingDirection, center, intersection.add(nextDirection.divide(nextDirection.norm())));
                        if(isShiftVectorOutsidePolygon != null)
                        {
                            shiftVector = tmpShiftVector.multiply(-1);
                            nextDirection = nextDirection.multiply(-1);
                        }
                        else
                        {
                            shiftVector = tmpShiftVector;
                        }
                    }
                    else
                    {
                        //is the new shifting vector pointing in the opposite direction?
                        if(tmpShiftVector.innerProduct(shiftVector) <0)
                           nextDirection =  nextDirection.multiply(-1);
                    }
                    double maxShiftingDistance =  tmpShiftVector.norm();
                    Vector nextWaypoint = nextDirection.multiply(shiftingSize/maxShiftingDistance).add(intersection);

                    waypointsForMission.add(new GpsCoordinate(nextWaypoint.add(centerOfMass)));
                    flyingDirection = flyingDirection.multiply(-1);


                    currentPos = nextWaypoint;


                    break;
                }
                else
                    intersection = null;
            }
            //we are done
            if (intersection == null)
                return;

        }
    }
    public ArrayList<GpsCoordinate>getWaypoints()
    {
        return waypointsForMission;
    }
    private boolean waypointIsOutsidePolygon(Vector wp)
    {
        for(int i =0;  i< polygonPoints.size();i++)
        {
            Vector lineSegmentStart;
            Vector lineSegmentEnd;
            if (i + 1 < polygonPoints.size()) {
                lineSegmentStart = polygonPoints.get(i);
                lineSegmentEnd = polygonPoints.get(i + 1);
            }
            else
            {
                lineSegmentStart = polygonPoints.get(i);
                lineSegmentEnd = polygonPoints.get(0);
            }
            if(LinearAlgebraHelper.getRayLineSegmentIntersection(wp,centerOfMass.subtract(wp),lineSegmentStart,lineSegmentEnd) !=null)
                return true;

        }
        return false;

    }

    public String toJson() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for(int i =0; i< waypointsForMission.size();i++)
        {

            sb.append("{lat:"+waypointsForMission.get(i).getLat()+",lng:"+waypointsForMission.get(i).getLng()+"}");
            sb.append(",");
        }
        sb.append("];");

        return sb.toString();
    }
    public String toCsv()
    {
        StringBuilder sb = new StringBuilder();
        for(GpsCoordinate coordinate : waypointsForMission)
        {
            sb.append(coordinate.getLat()+";"+coordinate.getLng()+";"+coordinate.getAlt()+"\n");
        }
        return sb.toString();

    }
    public double getHeading()
    {
        return waypointsForMission.get(0).CalculateHeading(waypointsForMission.get(1));
    }
}
