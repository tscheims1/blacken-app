package ch.tscheims.blackenapp;

import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;

/**
 * Created by tsche on 22.09.2017.
 */

public class LinearAlgebraHelper
{
    private static final double EPSILON = 0.000000000001;
    public static boolean onSegment(Vector a,Vector b, Vector c)
    {
        return c.subtract(a).add(c.subtract(b)).equals(b.subtract(a));

    }

    public static Vector getRayLineSegmentIntersection(Vector origin,Vector dir, Vector start, Vector end)
    {
        //first check if they intersects
        //(AB × BC) * (AB × BD) < 0, and
        //    (CD × DA) * (CD × DB) < 0



        Vector end1 = origin.add(dir.multiply(1000));
        Vector dir2 = end.subtract(start);

        boolean inproperIntersect = false;
        if(onSegment(origin,start,end1) || onSegment(origin,end,dir)
                || onSegment(start,origin,end)||
                onSegment(start,dir,end))
        {
            inproperIntersect = true;
        }


        double check1 = cross(dir,start.subtract(end1)).innerProduct(cross(dir,end.subtract(end1)));
        double check2 = cross(dir2,origin.subtract(end)).innerProduct(cross(dir2,end1.subtract(end)));
        if(!(check1 < 0 && check2 <0) && !inproperIntersect)
            return null;


        //first check if the vectors are parallel and there are no zero vectors
        Vector term1 =cross(origin.subtract(start),dir);
        Vector term2 = cross(dir2,dir);

        //numeric better than compare unit vectors
        if(term2.norm() == 0)
            return null;

        Vector term2Unit = term2.divide(term2.norm());
        Vector term1Unit = term1.divide(term1.norm());
        double paralellVector = term2Unit.innerProduct(term1Unit);

        //if they are parallel: calculate the intersection point
        double s = term1.norm()/term2.norm();



        //check if the intersection is within the line segment
        //return start.add(end.subtract(start).multiply(s));

        if(s >= -1.0&& s <= 1.0)
            return start.add(end.subtract(start).multiply(s));
        else
            return null;


    }
    public static Vector perp(Vector a)
    {
        Vector result = new BasicVector(3);
        result.set(0,-a.get(1));
        result.set(1,a.get(0));
        return result;

    }
    public static Vector cross(Vector a,Vector b)
    {
        Vector result = new BasicVector(3);
        result.set(0,a.get(1)*b.get(2) - a.get(2)*b.get(1));
        result.set(1,a.get(2)*b.get(0) - a.get(0)*b.get(2));
        result.set(2,a.get(0)*b.get(1) - a.get(1)*b.get(0));
        return result;

    }

}
