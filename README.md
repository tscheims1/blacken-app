# Blacken-App

An app for testing a systematic waypoint flight in a given area with a drone. The source code only works with the DJI Phantom 3 Professional drone.

## Installation

1. Download and install Android-Studio
2. Import Project
3. Compile and deploy


## Testing 

My advice: Use the "DJI PC Simulator" simulator.